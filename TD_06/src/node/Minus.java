package node;

import src.code.*;
import beaver.Symbol;


public class Minus extends Node {

    public Minus(Node left, Node right) {
        super(left, right);
    }

    //Constructor for the unary minus
    public Minus(Node left) {
        super(left);
    }

    public CodeList linearize() {
        CodeList code = new CodeList();

        if (this.right != null) {
            code.add(new CodeOpUnary("-", this.left.linearize().Display()));
        } else {
            code.add(new CodeOpBinary("-", this.left.linearize().Display(), this.right.linearize().Display()));
        }

        return code;
    }
}