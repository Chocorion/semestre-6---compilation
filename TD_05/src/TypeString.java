public class TypeString extends Type {

    public boolean equalsTo(Type type) {
        return (type instanceof TypeString);
    }

    public String toString() {
        return "string";
    }
}