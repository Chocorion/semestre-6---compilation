package src.code;

public class CodeOpUnary extends Code {
    private String op;
    private String var;

    public CodeOpUnary(String op, String var) {
        super();
        this.op = op;
        this.var = var;
    }

    public String Display() {
        return this.op + " " + this.var;
    }
}