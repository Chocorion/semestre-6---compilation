public class TypeFunction extends Type {

    TypeTuple args;
    Type returnType;

    public TypeFunction(TypeTuple args, Type returnType) {
        this.args = args;
        this.returnType = returnType;
    }

    private TypeTuple getArgs() {
        return this.args;
    }

    private Type getReturnType() {
        return this.returnType;
    }

    public boolean equalsTo(Type type) {
        if(!(type instanceof TypeFunction)) {
            return false;
        }

        if (!this.args.equalsTo(((TypeFunction)type).getArgs())) {
            return false;
        }

        return this.returnType.equalsTo(((TypeFunction)type).getReturnType());
    }

    public String toString() {
        return this.args.toString() + " -> " + this.returnType.toString();
    }
}