import beaver.Symbol;
import beaver.Scanner;

%%

%class ScannerLogic
%extends Scanner
%function nextToken
%type Symbol
%line
%column
%yylexthrow Scanner.Exception
%eofval{
	return new Symbol(Terminals.EOF);
%eofval}
%unicode

%%

	"∨"			{	System.out.print("OR "); 	return new Symbol(Terminals.OR,yyline,yycolumn);}
	"∧"			{	System.out.print("AND "); 	return new Symbol(Terminals.AND);}
	"¬"			{	System.out.print("NOT "); 	return new Symbol(Terminals.NOT);}
	"("			{	System.out.print("( "); 	return new Symbol(Terminals.OPEN);}
	")"			{	System.out.print(") "); 	return new Symbol(Terminals.CLOSE);}
	"<"			{	System.out.print("< ");		return new Symbol(Terminals.INF);}
	">"			{	System.out.print("> ");		return new Symbol(Terminals.SUPP);}
	"≤"			{	System.out.print("≤ ");		return new Symbol(Terminals.INF_EQUALS);}
	"≥"			{	System.out.print("≥ ");		return new Symbol(Terminals.SUPP_EQUALS);}
	"!="		{	System.out.print("!= ");	return new Symbol(Terminals.NOT_EQUALS);}
	"="			{	System.out.print("= ");		return new Symbol(Terminals.EQUALS);}
	"+"			{	System.out.print("+ ");		return new Symbol(Terminals.PLUS);}
	"*"			{	System.out.print("* ");		return new Symbol(Terminals.MULT);}
	"-"			{	System.out.print("- ");		return new Symbol(Terminals.MINUS);}
	"/"			{	System.out.print("/ ");		return new Symbol(Terminals.DIV);}

	"false"		{ 	System.out.print("False "); 	return new Attribute(Terminals.FALSE, false)	;}
	"true"		{ 	System.out.print("True "); 		return new Attribute(Terminals.TRUE,  true)		;}
	[0-9]+ 		{	System.out.print("variable (" + yytext() + ") "); 		return new Attribute(Terminals.VAR, new Double(yytext()));}
	

	\n          { }
	[^]         { }
