public class TypeInt extends Type {

    public boolean equalsTo(Type type) {
        return (type instanceof TypeInt);
    }
    
    public String toString() {
        return "int";
    }
}