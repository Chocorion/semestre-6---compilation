package src.code;
import java.util.*;


public class CodeList {
    static int cpt = 0;
    private ArrayList<Code> tab;
    
    public CodeList () {
        this.tab = new ArrayList<>();
    }


    private static String newIdent() {
        return "#t" + cpt++;
    }

    public int getLine() {
        //Plus tard penser aux goto, ajout de k + 1
        return this.tab.size();
    }

    public void add(Code c) {
        this.tab.add(c);
    }

    public void add(CodeList code) {
        for (Code c : code.tab) {
            this.add(c);
        }
    }

    public String Display() {
        if (this.tab.size() == 0)   return "";
        String dis = tab.get(0).Display();

        for (int i = 1; i < this.tab.size(); i++) {
            dis += " " + this.tab.get(i).Display();
        }

        return dis;
    }
}