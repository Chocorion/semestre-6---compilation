import beaver.Symbol;
import beaver.Scanner;

%%

%class ScannerExpr
%extends Scanner
%function nextToken
%type Symbol
%yylexthrow Scanner.Exception
%eofval{
	System.out.println("End of the file !");
	System.out.println(yytext()); return new Symbol(Terminals.EOF);
%eofval}
%unicode
%line
%column

Comm = \/\/~\n
Identifier = [a-zA-Z_][a-zA-Z0-9_]*
Integer = [0-9]+

%%

{Comm}	{}

"+" 	        { System.out.println("PLUS   -> " + yytext()); return new Symbol(Terminals.PLUS	, yyline, yycolumn); }
"-" 	        { System.out.println("MINUS  -> " + yytext()); return new Symbol(Terminals.MINUS, yyline, yycolumn); }
"*"				{ System.out.println("MULT	 -> " + yytext()); return new Symbol(Terminals.MULT, yyline, yycolumn);  }
"(" 	        { System.out.println("LPAR   -> " + yytext()); return new Symbol(Terminals.LPAR	, yyline, yycolumn); }
")" 	        { System.out.println("RPAR   -> " + yytext()); return new Symbol(Terminals.RPAR	, yyline, yycolumn); }
"=" 	        { System.out.println("EQUALS -> " + yytext()); return new Symbol(Terminals.EQUALS	, yyline, yycolumn); }
";" 	        { System.out.println("SEMI   -> " + yytext()); return new Symbol(Terminals.SEMI	, yyline, yycolumn); }
"int"			{ System.out.println("INT	 -> " + yytext()); return new Symbol(Terminals.INT_TYPE, yyline, yycolumn); }
{Integer}		{ System.out.println("INT    -> " + yytext()); return new Symbol(Terminals.valeur_entiere, yyline, yycolumn, new Integer(yytext())); }
{Identifier}	{ System.out.println("ID     -> " + yytext()); return new Symbol(Terminals.identificateur, yyline, yycolumn, yytext()); }

[^]|\n		{ }
