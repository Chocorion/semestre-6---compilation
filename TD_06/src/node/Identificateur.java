package node;

import beaver.Symbol;
import src.code.*;

public class Identificateur extends Node {
    protected String name;
    public Identificateur(String name) {
        super();
        this.name = name;
    }


    public String Display() {
        return "(" + this.name + ")";
    }
}