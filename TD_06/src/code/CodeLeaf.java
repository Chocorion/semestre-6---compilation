package src.code;

public class CodeLeaf extends Code {
    String value;
    
    public CodeLeaf(String value) {
        this.value = value;
    }

    public String Display() {
        return value;
    }
}