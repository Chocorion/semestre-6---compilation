package src.code;

import beaver.Symbol;

public abstract class Code implements CodeInt {

    static int id = 0;

    protected Code left;
    protected Code middle;
    protected Code right;

    // Constructor Code three value
    public Code(Code left, Code middle,Code right) {
        super();
        this.left = left;
        this.middle = middle;
        this.right = right;
    }
    // Constructor Code two value
    public Code(Code left, Code right) {
        super();
        this.left = left;
        this.right = right;
    }

    // Constructor Code one value
    public Code(Code left) {
        super();
        this.left = left;
    }

    // Contructor no value
    public Code() {
        super();
    }

    protected static int getId() {
        id++;
        return id;
    }

    // Affichage parenthésé de l'arbre
    public String Display() {
        if (left != null)
            if (right != null)
                return this.getClass().getSimpleName() + "(" + left.Display() + "," + right.Display() + ")";
            else
                return this.getClass().getSimpleName() + "(" + left.Display() + ")";
        else
            return this.getClass().getSimpleName();
    }

    
}
