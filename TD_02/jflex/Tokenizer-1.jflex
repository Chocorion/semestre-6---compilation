import java.util.*;


%%
%public
%class Tokenizer
%type Token
%line
%column

Keyword = bool
        | break
        | case
        | catch
        | char
        | class
        | const
        | continue
        | default
        | delete
        | do
        | double
        | else
        | enum
        | false
        | float
        | for
        | friend
        | goto
        | if
        | inline
        | int
        | long
        | namespace
        | new
        | operator
        | private
        | protected
        | public
        | register
        | return
        | short
        | signed
        | sizeof
        | static
        | struct
        | switch
        | template
        | this
        | throw
        | true
        | try
        | typedef
        | typeid
        | typename
        | union
        | unsigned
        | using
        | virtual
        | void
        | while

Operator      =    "++"
                |  "--"
                |  "&"
                |  "*"
                |  "+"
                |  "-"
                |  "~"
                |  "!"
                |  "/"
                |  "%"
                |  "<<"
                |  ">>"
                |  "<"
                |  ">"
                |  "<="
                |  ">="
                |  "=="
                |  "!="
                |  "^"
                |  "|"
                |  "&&"
                |  "||"
                |  "="
                |  "*="
                |  "/="
                |  "%="
                |  "+="
                |  "-="
                |  "<<="
                |  ">>="
                |  "&="
                |  "^="
                |  "|="

Separator      =   ","
                |  ";"
                |  ":"
                |  "("
                |  ")"
                |  "["
                |  "]"
                |  "."
                |  "{"
                |  "}"

%state comment
%state comment_doc

Chiffre        = [0-9]
Nombre         = {Chiffre}+
Nombre_signe   = -?{Nombre}
Flottant       = -?(([0-9]*\.({Nombre}([eE]-?{Nombre})?))|(({Nombre}\.)({Nombre}([eE]-?{Nombre})?)?))
Identificateur = [A-z_][A-z0-9_]*
String         = \"~\"
Commentaires   = \/\/.*+(\/\*~\*\/)
Documentation  = author|version|param|return

%%

<comment_doc> {
    {Documentation}~"\n" {
        System.out.println("\033[31mDocumentation : \033[0m" + yytext());
    }

   "*/"   {yybegin(YYINITIAL);}
    [^] {}
}

<comment> {
    "*/"    {
        System.out.println("Sortie du mode commentaire !");
        yybegin(YYINITIAL);
    }

    [^] {}
}

<YYINITIAL>
{
    \/\*            {
        System.out.println("État commentaire !");
        yybegin(comment);
    }
    "/**"   {
        System.out.println("Mode documentation !");
        yybegin(comment_doc);
    }

    bool            { return new Token(Sym.BOOL, null, yyline, yycolumn); }
    break           { return new Token(Sym.BREAK ,null, yyline, yycolumn); }
    case            { return new Token(Sym.CASE ,null, yyline, yycolumn); }
    catch           { return new Token(Sym.CATCH ,null, yyline, yycolumn); }
    char            { return new Token(Sym.CHAR ,null, yyline, yycolumn); }
    class           { return new Token(Sym.CLASS ,null, yyline, yycolumn); }
    const           { return new Token(Sym.CONST ,null, yyline, yycolumn); }
    continue        { return new Token(Sym.CONTINUE ,null, yyline, yycolumn); }
    default         { return new Token(Sym.DEFAULT ,null, yyline, yycolumn); }
    delete          { return new Token(Sym.DELETE ,null, yyline, yycolumn); }
    do              { return new Token(Sym.DO ,null, yyline, yycolumn); }
    double          { return new Token(Sym.DOUBLE ,null, yyline, yycolumn); }
    else            { return new Token(Sym.ELSE ,null, yyline, yycolumn); }
    enum            { return new Token(Sym.ENUM ,null, yyline, yycolumn); }
    false           { return new Token(Sym.FALSE ,null, yyline, yycolumn); }
    float           { return new Token(Sym.FLOAT ,null, yyline, yycolumn); }
    for             { return new Token(Sym.FOR ,null, yyline, yycolumn); }
    friend          { return new Token(Sym.FRIEND ,null, yyline, yycolumn); }
    goto            { return new Token(Sym.GOTO ,null, yyline, yycolumn); }
    if              { return new Token(Sym.IF ,null, yyline, yycolumn); }
    inline          { return new Token(Sym.INLINE ,null, yyline, yycolumn); }
    int             { return new Token(Sym.INT ,null, yyline, yycolumn); }
    long            { return new Token(Sym.LONG ,null, yyline, yycolumn); }
    namespace       { return new Token(Sym.NAMESPACE ,null, yyline, yycolumn); }
    new             { return new Token(Sym.NEW ,null, yyline, yycolumn); }
    operator        { return new Token(Sym.OPERATOR ,null, yyline, yycolumn); }
    private         { return new Token(Sym.PRIVATE ,null, yyline, yycolumn); }
    protected       { return new Token(Sym.PROTECTED ,null, yyline, yycolumn); }
    public          { return new Token(Sym.PUBLIC ,null, yyline, yycolumn); }
    register        { return new Token(Sym.REGISTER ,null, yyline, yycolumn); }
    return          { return new Token(Sym.RETURN ,null, yyline, yycolumn); }
    short           { return new Token(Sym.SHORT ,null, yyline, yycolumn); }
    signed          { return new Token(Sym.SIGNED ,null, yyline, yycolumn); }
    sizeof          { return new Token(Sym.SIZEOF ,null, yyline, yycolumn); }
    static          { return new Token(Sym.STATIC ,null, yyline, yycolumn); }
    struct          { return new Token(Sym.STRUCT ,null, yyline, yycolumn); }
    switch          { return new Token(Sym.SWITCH ,null, yyline, yycolumn); }
    template        { return new Token(Sym.TEMPLATE ,null, yyline, yycolumn); }
    this            { return new Token(Sym.THIS ,null, yyline, yycolumn); }
    throw           { return new Token(Sym.THROW ,null, yyline, yycolumn); }
    true            { return new Token(Sym.TRUE ,null, yyline, yycolumn); }
    try             { return new Token(Sym.TRY ,null, yyline, yycolumn); }
    typedef         { return new Token(Sym.TYPEDEF ,null, yyline, yycolumn); }
    typeid          { return new Token(Sym.TYPEID ,null, yyline, yycolumn); }
    typename        { return new Token(Sym.TYPENAME ,null, yyline, yycolumn); }
    union           { return new Token(Sym.UNION ,null, yyline, yycolumn); }
    unsigned        { return new Token(Sym.UNSIGNED ,null, yyline, yycolumn); }
    using           { return new Token(Sym.USING ,null, yyline, yycolumn); }
    virtual         { return new Token(Sym.VIRTUAL ,null, yyline, yycolumn); }
    void            { return new Token(Sym.VOID ,null, yyline, yycolumn); }
    while

    "++"            { return new Token(Sym.OPERATOR_PLUSPLUS , null, yyline, yycolumn); }
    "--"            { return new Token(Sym.OPERATOR_MINUSMINUS , null, yyline, yycolumn); }
    "&"             { return new Token(Sym.OPERATOR_AND , null, yyline, yycolumn); }
    "*"             { return new Token(Sym.OPERATOR_MULT , null, yyline, yycolumn); }
    "+"             { return new Token(Sym.OPERATOR_PLUS , null, yyline, yycolumn); }
    "-"             { return new Token(Sym.OPERATOR_MINUS , null, yyline, yycolumn); }
    "~"             { return new Token(Sym.OPERATOR_TILDE , null, yyline, yycolumn); }
    "!"             { return new Token(Sym.OPERATOR_EXCLAMATION , null, yyline, yycolumn); }
    "/"             { return new Token(Sym.OPERATOR_SLASH , null, yyline, yycolumn); }
    "%"             { return new Token(Sym.OPERATOR_PERCENT , null, yyline, yycolumn); }
    "<<"            { return new Token(Sym.OPERATOR_INFINF , null, yyline, yycolumn); }
    ">>"            { return new Token(Sym.OPERATOR_SUPPSUPP , null, yyline, yycolumn); }
    "<"             { return new Token(Sym.OPERATOR_INF , null, yyline, yycolumn); }
    ">"             { return new Token(Sym.OPERATOR_SUPP , null, yyline, yycolumn); }
    "<="            { return new Token(Sym.OPERATOR_INFEQUALS , null, yyline, yycolumn); }
    ">="            { return new Token(Sym.OPERATOR_SUPPEQUALS , null, yyline, yycolumn); }
    "=="            { return new Token(Sym.OPERATOR_EQUALSEQUALS , null, yyline, yycolumn); }
    "!="            { return new Token(Sym.OPERATOR_NOTEQUALS , null, yyline, yycolumn); }
    "^"             { return new Token(Sym.OPERATOR_CHAPEAU , null, yyline, yycolumn); }
    "|"             { return new Token(Sym.OPERATOR_PIPE , null, yyline, yycolumn); }
    "&&"            { return new Token(Sym.OPERATOR_ANDAND , null, yyline, yycolumn); }
    "||"            { return new Token(Sym.OPERATOR_PIPEPIPE , null, yyline, yycolumn); }
    "="             { return new Token(Sym.OPERATOR_EQUALS , null, yyline, yycolumn); }
    "*="            { return new Token(Sym.OPERATOR_MULTEQUALS , null, yyline, yycolumn); }
    "/="            { return new Token(Sym.OPERATOR_SLASHEQUALS , null, yyline, yycolumn); }
    "%="            { return new Token(Sym.OPERATOR_PERCENTEQUALS , null, yyline, yycolumn); }
    "+="            { return new Token(Sym.OPERATOR_PLUSEQUALS , null, yyline, yycolumn); }
    "-="            { return new Token(Sym.OPERATOR_LESSEQUALS , null, yyline, yycolumn); }
    "<<="           { return new Token(Sym.OPERATOR_LESSLESSEQUALS , null, yyline, yycolumn); }
    ">>="           { return new Token(Sym.OPERATOR_SUPPSUPPEQUALS , null, yyline, yycolumn); }
    "&="            { return new Token(Sym.OPERATOR_ANDEQUALS , null, yyline, yycolumn); }
    "^="            { return new Token(Sym.OPERATOR_CHAPEAUEQUALS , null, yyline, yycolumn); }
    "|="            { return new Token(Sym.OPERATOR_PIPEEQUALS , null, yyline, yycolumn); }

    ","             { return new Token(Sym.SEPARATOR_VIRGULE , null, yyline, yycolumn); }
    ";"             { return new Token(Sym.SEPARATOR_DOTVIRGULE , null, yyline, yycolumn); }
    ":"             { return new Token(Sym.SEPARATOR_TWODOTS , null, yyline, yycolumn); }
    "("             { return new Token(Sym.SEPARATOR_OPENPAR , null, yyline, yycolumn); }
    ")"             { return new Token(Sym.SEPARATOR_CLOSEPAR , null, yyline, yycolumn); }
    "["             { return new Token(Sym.SEPARATOR_OPENCROCHET , null, yyline, yycolumn); }
    "]"             { return new Token(Sym.SEPARATOR_CLOSECROCHET , null, yyline, yycolumn); }
    "."             { return new Token(Sym.SEPARATOR_DOT , null, yyline, yycolumn); }
    "{"             { return new Token(Sym.SEPARATOR_OPENACC , null, yyline, yycolumn); }
    "}"             { return new Token(Sym.SEPARATOR_CLOSEACC , null, yyline, yycolumn); }

    {Identificateur} {
        System.out.println("Identificateur --> " + yytext());
    }

    [^] {}
}
