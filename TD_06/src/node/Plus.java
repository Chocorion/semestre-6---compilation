package node;
import beaver.Symbol;
import src.code.*;
public class Plus extends Node {
    public Plus(Node left, Node right) {
        super(left, right);
    }

    public Plus(Node left) {
        super(left);
    }

    public CodeList linearize() {
        CodeList code = new CodeList();

        if (this.right != null) {
            code.add(new CodeOpUnary("+", this.left.linearize().Display()));
        } else {
            code.add(new CodeOpBinary("+", this.left.linearize().Display(), this.right.linearize().Display()));
        }

        return code;
    }
}