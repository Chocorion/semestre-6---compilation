public class TypeList extends Type {

    Type type;

    public TypeList(Type type) {
        this.type = type;
    }

    public boolean equalsTo(Type type) {
        return (type instanceof TypeList);
    }

    public String toString() {
        return "List(" + this.type.toString() + ')';
    }
}