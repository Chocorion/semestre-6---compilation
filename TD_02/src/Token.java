public class Token {
    private Sym sym;
    private Object value;
    private Integer lineno;
    private Integer colno;

    public Token(Sym sym, Object value, int lineno, int colno) {
        this.sym = sym;
        this.value = value;
        this.lineno = lineno;
        this.colno = colno;
    }

    public String toString() {
        return this.sym.toString();
    }
}
