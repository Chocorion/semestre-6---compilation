import java.util.*;

public class TypeTuple extends Type {

    ArrayList<Type> types;

    public TypeTuple(Type... types) {
        this.types = new ArrayList<>();
        for (Type t : types) {
            this.types.add(t);
        }
    }

    public TypeTuple(ArrayList<Type> a) {
        this.types = new ArrayList<>();
        this.types = a;
    }

    public ArrayList<Type> getTypes() {
        return this.types;
    }

    public boolean equalsTo(Type type) {
        
        if (!(type instanceof TypeTuple) || this.types.size() != (((TypeTuple) type).getTypes()).size()) {
            return false;
        }

        List<Type> types2 = ((TypeTuple) type).getTypes();
        for (int i = 0; i < this.types.size(); i++) {
            if (!types.get(i).equalsTo(types2.get(i))) {
                return false;
            }
        }

        return true;
    }

    public String toString() {
        String ret = "(";

        for (int i = 0; i < this.types.size(); i++) {
            ret += this.types.get(i).toString();

            if (i < this.types.size() - 1) {
                ret += ", ";
            }
        }

        return ret + ")";
    }

    public void add(Type e) {
        this.types.add(e);
    }
}