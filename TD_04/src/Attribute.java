public class Attribute extends beaver.Symbol {
    private boolean bValue;
    private double dValue;

    boolean getbValue() {
        return bValue;
    }

    double getdValue() {
        return dValue;
    }

    Attribute(boolean value) {
        bValue = value;
    }

    Attribute(double value) {
        dValue = value;
    }

    Attribute(short t, boolean value) {
        super(t);
        bValue = value;
    }

    Attribute(short t, double value) {
        super(t);
        dValue = value;
    }
}