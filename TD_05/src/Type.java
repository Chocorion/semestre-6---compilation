import beaver.Symbol;

// import java.util.*;

// public class Type extends beaver.Symbol {
//     private static int id;
//     private static Map<String, Integer> map;
    
//     private int typeId;

//     static {
//         id = 0;
//         map = new HashMap<>();
//     }

//     public Type(String typeName) {
//         if (map.containsKey(typeName)) {
//             this.typeId = map.get(typeName);
//         } else {
//             map.put(typeName, id);
//             id++;
//         }
//     }

//     public int getTypeId() {
//         return this.typeId;
//     }

//     public boolean isEquals(Type type) {
//         return type.getTypeId() == this.typeId;
//     } 
// }

public abstract class Type extends beaver.Symbol {
    public abstract String toString();
    public abstract boolean equalsTo(Type type);
}