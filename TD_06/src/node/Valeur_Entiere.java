package node;
import beaver.Symbol;
import src.code.*;
public class Valeur_Entiere extends Node {
    private int value;
    
    public Valeur_Entiere(int v) {
        super();
        this.value = v;
    }

    public String Display() {
        return "(" + value + ")";
    }

    public CodeList linearize() {
        CodeList code = new CodeList();
        code.add(new CodeLeaf(Integer.toString(this.value)));

        return code;
    }
}