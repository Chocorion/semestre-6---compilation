package src.code;

public class CodeOpBinary extends Code {
    private String op;
    private String var1;
    private String var2;

    public CodeOpBinary(String op, String var1, String var2) {
        super();

        this.op = op;
        this.var1 = var1;
        this.var2 = var2;
    }

    public String Display() {
        return this.var1 + " " + this.op + " " + this.var2;
    }
}