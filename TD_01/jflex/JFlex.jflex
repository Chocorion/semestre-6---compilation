import java.io.*;

%%

%public
%class Lexer
%standalone
%8bit

%state comment

%{
    int number_line = 0;
    int number_keyword = 0;
    int number_id = 0;
%}

%eof{
    System.out.println("Nombre de lignes        : " + number_line);
    System.out.println("Nombre de mot clef      : " + number_keyword);
    System.out.println("Nombre d'identificateur : " + number_id);
%eof}

Keyword = bool
        | break
        | case
        | catch
        | char
        | class
        | const
        | continue
        | default
        | delete
        | do
        | double
        | else
        | enum
        | false
        | float
        | for
        | friend
        | goto
        | if
        | inline
        | int
        | long
        | namespace
        | new
        | operator
        | private
        | protected
        | public
        | register
        | return 
        | short
        | signed
        | sizeof
        | static
        | struct
        | switch
        | template
        | this
        | throw
        | true
        | try
        | typedef
        | typeid
        | typename
        | union
        | unsigned
        | using
        | virtual        
        | void
        | while

Operator      =   "++"
                |  "--"
                |  "&"
                |  "*"
                |  "+"
                |  "-"
                |  "~"
                |  "!"
                |  "/"
                |  "%"
                |  "<<"
                |  ">>"
                |  "<"
                |  ">"
                |  "<="
                |  ">="
                |  "=="
                |  "!="
                |  "^"
                |  "|"
                |  "&&"
                |  "||"
                | "="
                |  "*="
                |  "/="
                |  "%="
                |  "+="
                |  "-="
                |  "<<="
                |  ">>="
                |  "&="
                |  "^="
                |  "|="

Separator      =   ","
                |  ";"
                |  ":"
                |  "("
                |  ")"
                |  "["
                |  "]"
                |  "."
                |  "{"
                |  "}"
                

Chiffre        = [0-9]
Nombre         = {Chiffre}+
Nombre_signe   = -?{Nombre}
Flottant       = -?(([0-9]*\.({Nombre}([eE]-?{Nombre})?))|(({Nombre}\.)({Nombre}([eE]-?{Nombre})?)?))
Identificateur = [A-z_][A-z0-9_]*
String         = \"~\"
Commentaires   = \/\/.*
Bloc_comm      = (\/\*)
%%

<comment>{
    \\n  {number_line++;}
    \*\/  {yybegin(YYINITIAL);}
}


{Keyword}           {System.out.printf("\033[31mKeyword :\033[0m         %s\n", yytext());number_keyword++;}
{Identificateur}    {System.out.printf("\033[34mIdentificateur :\033[0m  %s\n", yytext());number_id++;}
{Nombre_signe}      {System.out.printf("\033[32mNombre         :\033[0m  %s\n", yytext());}
{Flottant}          {System.out.printf("\033[33mFlottant       :\033[0m  %s\n", yytext());}
{Separator}         {System.out.printf("\033[36mSeparator      :\033[0m  %s\n", yytext());}
{String}            {System.out.printf("\033[1;33mString         :\033[0m  %s\n", yytext());}
{Commentaires}      {System.out.printf("\033[1;33mCommentaires      :\033[0m  %s\n", yytext());}
{Operator}          {System.out.printf("\033[35mOperator       :\033[0m  %s\n", yytext());}
{Bloc_comm}         {yybegin(comment);}
/* Operators */

[\n]                {number_line++;}

[^]|\n {}
