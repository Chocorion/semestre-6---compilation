package src.code;

public class CodeCopy extends Code {
    private String var1;
    private String var2;

    public CodeCopy(String var1, String var2) {
        super();
        this.var1 = var1;
        this.var2 = var2;
    }

    public String Display() {
        return "(" + this.var1 + " = " + this.var2 + ")";
    }
}