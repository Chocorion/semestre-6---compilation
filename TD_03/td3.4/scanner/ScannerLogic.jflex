import beaver.Symbol;
import beaver.Scanner;

%%

%class ScannerLogic
%extends Scanner
%function nextToken
%type Symbol
%line
%column
%yylexthrow Scanner.Exception
%eofval{
	return new Symbol(Terminals.EOF);
%eofval}
%unicode

%%

	"∨"			{	System.out.print("OR "); 	return new Symbol(Terminals.OR);}
	"∧"			{	System.out.print("AND "); 	return new Symbol(Terminals.AND);}
	"¬"			{	System.out.print("NOT "); 	return new Symbol(Terminals.NOT);}
	"("			{	System.out.print("( "); 	return new Symbol(Terminals.OPEN);}
	")"			{	System.out.print(") "); 	return new Symbol(Terminals.CLOSE);}

	"0"				{ 	System.out.print("False "); 	return new Symbol(Terminals.FALSE);}
	"1"				{ 	System.out.print("True "); 		return new Symbol(Terminals.TRUE);}
	[a-zA-Z0-9	_]+ {	System.out.print("VAR "); 		return new Symbol(Terminals.VAR);}

	\n          { 	 }
	[^]         { }
