package node;
import src.code.*;


public class Affectation extends Node {
    public Affectation(Node left, Node right) {
        super(left, right);
    }

    public CodeList linearize() {
        CodeList code = new CodeList();
        code.add(
            new CodeCopy(
                this.left.Display(),
                this.right.linearize().Display()
            )
        );

        return code;
    }
}