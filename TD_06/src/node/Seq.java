package node;
import beaver.Symbol;
import src.code.*;
public class Seq extends Node {
    
    public Seq(Node left, Node right) {
        super(left, right);
    }
    
    public CodeList linearize() {
        CodeList left = this.left.linearize();
        left.add(this.right.linearize());
        return left;
    }
}