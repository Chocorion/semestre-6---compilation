package node;
import beaver.Symbol;
import src.code.*;

public abstract class Node extends Symbol implements NodeInt {

	protected Node left;
	protected Node right;

	// Constructor node two value
	public Node(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	// Constructor node one value
	public Node(Node left) {
		super();
		this.left = left;
	}

	// Contructor no value
	public Node() {
		super();
	}


	// Affichage parenthésé de l'arbre
	public String Display() {
		if (left != null)
			if (right != null)
				return this.getClass().getSimpleName() + "(" + left.Display() + "," + right.Display() + ")";
			else
				return this.getClass().getSimpleName() + "(" + left.Display() + ")";
		else
			return this.getClass().getSimpleName();
	}

	public CodeList linearize() {
		return new CodeList();
	}

	
}
