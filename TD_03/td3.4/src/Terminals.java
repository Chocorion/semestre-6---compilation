/**
 * This class lists terminals used by the
 * grammar specified in "ParserLogic.grammar".
 */
public class Terminals {
	static public final short EOF = 0;
	static public final short OPEN = 1;
	static public final short VAR = 2;
	static public final short TRUE = 3;
	static public final short FALSE = 4;
	static public final short NOT = 5;
	static public final short CLOSE = 6;
	static public final short OR = 7;
	static public final short AND = 8;
}
