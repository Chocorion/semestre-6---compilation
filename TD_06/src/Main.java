
import java.io.*;

import node.*;
import src.code.*;
public class Main {
	public static void main(String[] args) throws Exception {
		ScannerExpr input = new ScannerExpr(new FileReader(args[0]));
		ParserExpr parser = new ParserExpr();

		Node expr = (Node)parser.parse(input);
		System.out.println(expr.Display());

		System.out.println(expr.linearize().Display());
	}
}
