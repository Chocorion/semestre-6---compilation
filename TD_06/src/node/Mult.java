package node;

import src.code.*;

public class Mult extends Node {
    public Mult(Node left, Node right) {
        super(left, right);
    }

    public CodeList linearize() {
        CodeList c = new CodeList();

        c.add(
            new CodeOpBinary("*", this.left.linearize().Display(), this.right.linearize().Display())
        );

        return c;
    }
}